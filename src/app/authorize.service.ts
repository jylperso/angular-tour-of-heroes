import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizeService {

  constructor(private http:HttpClient) { }

  async getToken(server:string, login: string, password:string) {
    return new Promise((resolve, reject) => {
      let body={login, password};
      this.http.post(server,body).subscribe(result=>{
        resolve(result);
      });  
    })
  }

  getOToken(server:string, login:string, password:string):Observable<any> {
    let body={login, password};
    return this.http.post<any>(server,body);
  }


  getGroups(token:string, rootServer: string):Observable<any> {
    let url = rootServer + 'config/groups';
    return this.http.get<any>(url,{
      headers: {
        "Authorization": "Bearer " + token
      }
    });
  }
}
