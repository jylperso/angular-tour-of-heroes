export interface BoardStat {
    status: string,
    message: string,
    result: number,
    nobike: number,
    bike: number
}
