import { Component, OnInit } from '@angular/core';
import { AuthorizeService } from '../authorize.service';

@Component({
  selector: 'app-xuc',
  templateUrl: './xuc.component.html',
  styleUrls: ['./xuc.component.css']
})
export class XucComponent implements OnInit {
  rootServer:string ='https://10.169.212.59/xuc/api/2.0/' 
  server:string = this.rootServer + '/auth/login';
  username:string = 'test-xuc';
  password:string = '123456';
  authResult!:{login:string, token:string};
  groups?:string;

  constructor(private authorizeService:AuthorizeService) { }


  submit() {
    this.authorizeService.getToken(this.server, this.username, this.password).then(result =>{
      this.authResult = result as {login:string, token:string};
    } )
  }

  oSubmit() {
    this.authorizeService.getOToken(this.server, this.username, this.password).subscribe(result =>{
      this.authResult = result as {login:string, token:string};
    });
  }

  getGroupsSubmit() {
    this.authorizeService.getGroups(this.authResult.token, this.rootServer).subscribe(result =>{
      this.groups = result as string;
    });

  }
  
  ngOnInit(): void {
  }

}
