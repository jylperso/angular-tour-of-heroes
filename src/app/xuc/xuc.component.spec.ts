import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XucComponent } from './xuc.component';

describe('XucComponent', () => {
  let component: XucComponent;
  let fixture: ComponentFixture<XucComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XucComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
