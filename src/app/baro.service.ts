import { Injectable } from '@angular/core';
import { catchError, Observable, of, tap } from 'rxjs';
import { BoardStat } from './board-stat';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from './message.service';


@Injectable({
  providedIn: 'root'
})
export class BaroService {

  private boardStatUrl = 'https://barometre.parlons-velo.fr/api/4cds56c4sdc4c56ds4cre84c13ez8c4ezc6eza9c84ze16464cdsc1591cdzf8ez/stats/total?false';  // URL to web api

  constructor(private http: HttpClient,
    private messageService: MessageService) { }


  getStats(): Observable<BoardStat> {
    return this.http.get<BoardStat>(this.boardStatUrl)
      .pipe(
        tap(_ => this.log('BoardStatService: fetched statistics from ' + this.boardStatUrl)),
        catchError(this.handleError<BoardStat>('getStats', <BoardStat>{}))
      );
  }
  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
