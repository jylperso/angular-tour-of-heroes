import { TestBed } from '@angular/core/testing';

import { BaroService } from './baro.service';

describe('BaroService', () => {
  let service: BaroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BaroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
