import { Component, OnInit } from '@angular/core';
import { BaroService } from '../baro.service';
import { BoardStat } from '../board-stat';

@Component({
  selector: 'app-baro-dashboard',
  templateUrl: './baro-dashboard.component.html',
  styleUrls: ['./baro-dashboard.component.css']
})
export class BaroDashboardComponent implements OnInit {

  boardStat: BoardStat = <BoardStat>{result: 0};

  constructor(private baroService: BaroService) { }


  getBoardStat() {
    this.baroService.getStats().subscribe(
      boardStat => this.boardStat = boardStat
    )
  }

  ngOnInit(): void {
  }

}
