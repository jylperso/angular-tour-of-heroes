import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaroDashboardComponent } from './baro-dashboard.component';

describe('BaroDashboardComponent', () => {
  let component: BaroDashboardComponent;
  let fixture: ComponentFixture<BaroDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaroDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaroDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
